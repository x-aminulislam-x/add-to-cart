import Pagination from "rc-pagination";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import forBrokenImg from "../../@assets/download.png";
import { AppService } from "../../@services/api/App.service";
import { addToCart } from "../../store/reducers/OrdersReducer";
import "./Products.scss";

const Products = () => {
  const dispatch = useDispatch();

  const [products, setProducts] = useState([]);

  const navigate = useNavigate();

  //   const { products } = useSelector((state): any => state);

  useEffect(() => {
    AppService.getProducts().then((res) => setProducts(res));
    // dispatch(fetchProducts());
  }, []);

  const [perPage, setPerPage] = useState(10);
  const [size, setSize] = useState(perPage);
  const [current, setCurrent] = useState(1);

  const PerPageChange = (value) => {
    setSize(value);
    const newPerPage = Math.ceil(products.length / value);
    if (current > newPerPage) {
      setCurrent(newPerPage);
    }
  };

  const getData = (current, pageSize) => {
    // Normally you should get the data from the server
    return products.slice((current - 1) * pageSize, current * pageSize);
  };

  const PaginationChange = (page, pageSize) => {
    setCurrent(page);
    setSize(pageSize);
  };

  const PrevNextArrow = (current, type, originalElement) => {
    if (type === "prev") {
      return <button>Pre</button>;
    }
    if (type === "next") {
      return <button>Next</button>;
    }
    return originalElement;
  };

  return (
    <div className="products-main">
      <div className="container">
        <Pagination
          className="pagination-data"
          showTotal={(total, range) =>
            `Showing ${range[0]}-${range[1]} of ${total}`
          }
          onChange={PaginationChange}
          total={products.length}
          current={current}
          pageSize={size}
          showSizeChanger={false}
          itemRender={PrevNextArrow}
          onShowSizeChange={PerPageChange}
        />
        <div className="products-wrapper  my-5">
          {getData(current, size).map((item) => {
            return (
              <div
                className="card product-item"
                style={{ width: "18rem" }}
                key={item?._id}
                onClick={() =>
                  navigate(`/products/${item?._id}`, { state: item })
                }
              >
                <img
                  src={item?.picture}
                  className="card-img-top"
                  alt="..."
                  onError={({ currentTarget }) => {
                    currentTarget.onerror = null; // prevents looping
                    currentTarget.src = forBrokenImg;
                  }}
                />
                <div className="card-body">
                  <h5 className="card-title">{item?.title}</h5>
                  <div className="d-flex justify-content-between align-items-center">
                    <div>
                      <span className="text-secondary">
                        Stock : {item?.stock}
                      </span>
                      <span className="text-success ms-2">
                        Price : ${item?.price}
                      </span>
                    </div>
                    <button
                      onClick={(e) => {
                        e.stopPropagation();
                        dispatch(addToCart(item));
                      }}
                      className="btn add-to-cart rounded text-white"
                    >
                      Add to cart
                    </button>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Products;
