import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import forBrokenImg from "../../@assets/download.png";
import { addToCart } from "../../store/reducers/OrdersReducer";
import "./ProductsDetails.scss";

const ProductsDetails = () => {
  const dispatch = useDispatch();
  const { orders }: any = useSelector((state: any) => state.orders);
  const { state } = useLocation();
  const [product, setProduct] = useState(state);

  useEffect(() => {
    const [foundProduct] = orders.filter((item) => item?._id === state?._id);
    setProduct(foundProduct);
  }, [orders]);

  return (
    <div className="product-details-main">
      <div className="product-details-wrapper container my-5">
        <div className="d-flex w-100 mb-4 pb-4 border-3 border-bottom">
          <div className="product-image w-50">
            <img
              src={state?.picture}
              alt=""
              onError={({ currentTarget }) => {
                currentTarget.onerror = null; // prevents looping
                currentTarget.src = forBrokenImg;
              }}
            />
          </div>
          <div className="ms-4 w-50">
            <h2 className="text-start">{state?.title}</h2>
            <h5 className="text-danger">${state?.price}</h5>
            <p>{state?.description}</p>
            <div>
              <span className="border p-3 rounded">
                {product?.quantity_addToCart || 0}
              </span>
              <button
                onClick={() => dispatch(addToCart(state))}
                className="btn btn-dark p-3 ms-3"
              >
                Add to cart
              </button>
            </div>
          </div>
        </div>
        <div>
          <h2>Description</h2>
          <br />
          <p>{state?.description}</p>
          <br />
          <p>{state?.description}</p>
          <br />
          <p>{state?.description}</p>
          <br />
        </div>
      </div>
    </div>
  );
};

export default ProductsDetails;
