import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Swal from "sweetalert2";
import { deleteItem, updateQuantity } from "../../store/reducers/OrdersReducer";

const PlaceOrder = () => {
  const dispatch = useDispatch();

  const { orders } = useSelector((state: any) => state.orders);
  const [subtotal, setSubtotal] = useState(0);

  useEffect(() => {
    if (orders.length)
      setSubtotal(
        orders
          .map((item) => item?.quantity_addToCart * item?.price)
          .reduce((curr, next) => curr + next)
      );
  }, [orders]);

  const handleConfirmOrder = () => {
    Swal.fire("Thank You!", "You order has been confirmed!", "success");
    setTimeout(() => window.location.replace("/"), 1500);
  };

  return (
    <div className="place-order-main">
      <div className="place-order-wrapper container">
        {orders.length ? (
          <>
            <div className="add-to-cart-table my-5">
              <table className="table border">
                <thead>
                  <tr>
                    <th scope="col">Title</th>
                    <th scope="col">Price</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Subtotal</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                  {orders.map((item) => {
                    return (
                      <tr key={item._id}>
                        <th scope="row">{item?.title}</th>
                        <td>{item?.price}</td>
                        <td>
                          <input
                            className="w-25 form-control"
                            type="number"
                            defaultValue={item?.quantity_addToCart}
                            min={0}
                            max={item?.stock}
                            onKeyPress={(e) => e.preventDefault()}
                            onChange={(e: any) =>
                              dispatch(
                                updateQuantity({
                                  ...item,
                                  quantity_addToCart: e.target.value,
                                })
                              )
                            }
                          />
                        </td>
                        <td>{item?.quantity_addToCart * item?.price}</td>
                        <td>
                          <button
                            className="btn btn-danger"
                            onClick={() => dispatch(deleteItem(item))}
                          >
                            Delete
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
            <div className="d-flex align-items-center">
              <div className="cart-totals w-50 d-flex flex-column justify-content-end border border-2 p-2 rounded">
                <div className="d-flex my-2">
                  <div className="fw-bold w-25  text-start">Subtotal</div>
                  <div>{subtotal}</div>
                </div>
                <div className="d-flex my-2">
                  <div className="fw-bold w-25 text-start">Shipping</div>
                  <div>Free Shipping </div>
                </div>
                <div className="d-flex my-2">
                  <div className="fw-bold w-25  text-start">Total</div>
                  <div>{subtotal}</div>
                </div>
              </div>
              <button
                className="btn btn-success ms-5"
                onClick={handleConfirmOrder}
              >
                Confirm
              </button>
            </div>
          </>
        ) : (
          <h4 className="text-center text-danger my-5 ">
            No Product added into cart
          </h4>
        )}
      </div>
    </div>
  );
};

export default PlaceOrder;
