// import {AppService} from "@services/api/App.service.ts";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Cart from "./@components/Cart/Cart";
import Navbar from "./@components/Navbar/Navbar";
import PlaceOrder from "./@pages/PlaceOrder/PlaceOrder";
import Products from "./@pages/Products/Products";
import ProductsDetails from "./@pages/ProductsDetails/ProductsDetails";
import "./App.css";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Navbar />
        <Cart />
        <Routes>
          <Route path="/" element={<Products />} />
          <Route path="/place-order" element={<PlaceOrder />} />
          <Route path="/products/:id" element={<ProductsDetails />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
