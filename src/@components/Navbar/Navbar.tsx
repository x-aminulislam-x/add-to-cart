import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Link, useLocation } from "react-router-dom";

const Navbar = () => {
  const { orders }: any = useSelector((state: any) => state.orders);
  const [total, setTotal] = useState<number>(0);

  const { pathname } = useLocation();

  useEffect(() => {
    if (!orders.length) return;
    setTotal(0);
    // calculating total how many items added in cart
    setTotal(
      orders
        .map((item) => item.quantity_addToCart)
        .reduce((curr, pre) => curr + pre)
    );
  }, [orders]);

  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <Link className="navbar-brand" to="/">
            Navbar
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
              <li className="nav-item">
                <Link className="nav-link active" aria-current="page" to="/">
                  Home
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/place-order">
                  Place Orders
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/pricing">
                  Pricing
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/about" aria-disabled="true">
                  About
                </Link>
              </li>
            </ul>
          </div>
          {pathname !== "/place-order" ? (
            <p className="">
              Added to Cart :{" "}
              <span className="text-danger fw-bold">{total}</span>
            </p>
          ) : (
            ""
          )}
        </div>
      </nav>
    </div>
  );
};

export default Navbar;
