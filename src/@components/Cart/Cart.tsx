import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import forBrokenImg from "../../@assets/download.png";
import { deleteItem } from "../../store/reducers/OrdersReducer";
import "./Cart.scss";

const Cart = () => {
  const dispatch = useDispatch();
  const { orders }: any = useSelector((state: any) => state.orders);

  if (!orders.length) return;

  return (
    <div className="cart-main bg-light text-info p-3 rounded">
      {orders.map((item) => {
        return (
          <div className="cart-item border-bottom d-flex justify-content-between align-items-center py-2">
            <span
              className="mx-2 close"
              onClick={() => dispatch(deleteItem(item))}
            >
              &times;
            </span>
            <img
              src={item?.picture}
              alt=""
              onError={({ currentTarget }) => {
                currentTarget.onerror = null; // prevents looping
                currentTarget.src = forBrokenImg;
              }}
              className="w-25"
            />
            <span className="mx-2">{item?.title}</span>
          </div>
        );
      })}
      <button className="btn btn-success mt-2 d-flex justify-content-end">
        <Link to="place-order" className="text-white">
          Checkout
        </Link>
      </button>
    </div>
  );
};

export default Cart;
