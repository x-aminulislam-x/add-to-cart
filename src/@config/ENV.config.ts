interface IENV {
  ApiEndPoint: string;
}

export const ENV: IENV = {
  ApiEndPoint: process.env.REACT_APP_BASE_URL,
};
