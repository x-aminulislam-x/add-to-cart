import { apiIns } from "../../@config/api.config";

export const AppService = {
  getProducts: async (): Promise<any> => await apiIns.get("/Products"),
};
