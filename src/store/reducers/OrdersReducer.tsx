import { createSlice } from "@reduxjs/toolkit";

// created interface
interface IInitialState {
  orders: any;
  loading: "idle" | "pending" | "succeeded" | "failed";
}

// initial state
const initialState: IInitialState = {
  orders: [],
  loading: "idle",
};

export const ordersSlice = createSlice({
  name: "orders",
  initialState,
  reducers: {
    addToCart: ({ orders }, { payload }) => {
      // checking item is already in cart or not
      const itemInCart = orders.find(
        (item: any) => item.title === payload.title
      );
      if (itemInCart) {
        // checking items available quantity before adding in cart
        if (itemInCart.stock <= itemInCart.quantity_addToCart) {
          alert("No Available Quantity. All Quantity already added in cart");
          return;
        }
        // increasing add to cart quantity
        itemInCart.quantity_addToCart += 1;
        // decreasing available quantity
        itemInCart.quantity_available -= 1;
      } else {
        // if the item is not in cart then directly adding in cart
        orders.push({
          ...payload,
          quantity_addToCart: 1,
          quantity_available: payload.stock - 1,
        });
      }
    },
    updateQuantity: ({ orders }, { payload }) => {
      // checking item is already in cart or not
      const itemInCart = orders.find(
        (item: any) => item.title === payload.title
      );
      if (itemInCart) {
        // increasing add to cart quantity
        itemInCart.quantity_addToCart = payload.quantity_addToCart;
        // decreasing available quantity
        itemInCart.quantity_available =
          itemInCart.quantity_available > payload.quantity_addToCart
            ? itemInCart.quantity_available - payload.quantity_addToCart
            : payload.quantity_addToCart - itemInCart.quantity_available;
      }
    },

    deleteItem: ({ orders }, { payload }) => {
      orders.splice(
        orders.findIndex((item) => item._id === payload._id),
        1
      );
    },
  },
});

// exporting the action method
export const { addToCart, updateQuantity, deleteItem } = ordersSlice.actions;
export default ordersSlice.reducer;
