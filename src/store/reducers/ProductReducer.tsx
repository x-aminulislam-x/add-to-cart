import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { AppService } from "../../@services/api/App.service";

interface IInitialState {
  products: any;
  loading: "idle" | "pending" | "succeeded" | "failed";
}

const initialState: IInitialState = {
  products: [],
  loading: "idle",
};

// creating async call to fetch the products
export const fetchProducts: any = createAsyncThunk(
  "products",
  async (thunkAPI) => {
    try {
      // try to fetch products
      return await AppService.getProducts().then((res) => res);
    } catch (err) {
      // if fail then return the error
      return err;
    }
  }
);

export const productsSlice = createSlice({
  name: "products",
  initialState,
  reducers: {
    setProductsReducer: (state, action) => {
      return action.payload;
    },
  },
  extraReducers: (builder) => {
    // if the promise is pending
    builder.addCase(fetchProducts.pending, (state, { payload }) => {
      state.loading = "pending";
    });
    // if the promise is fulfilled payload will be added in initial state
    builder.addCase(fetchProducts.fulfilled, (state, { payload }) => {
      state.loading = "succeeded";
      state.products = payload;
      localStorage.setItem("products", JSON.stringify(payload));
    });
    // if the promise is rejected
    builder.addCase(fetchProducts.rejected, (state, { payload }) => {
      state.loading = "failed";
    });
  },
});

export const { setProductsReducer } = productsSlice.actions;

export default productsSlice.reducer;
