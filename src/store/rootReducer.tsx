import { combineReducers } from "@reduxjs/toolkit";
import OrdersReducer from "./reducers/OrdersReducer";
import { setProductsReducer } from "./reducers/ProductReducer";

// interface for reducers
export interface SessionState {
  products: any;
  orders: any;
}

// combining the reducers for multiple reducers
const rootReducer = combineReducers<SessionState>({
  products: setProductsReducer,
  orders: OrdersReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
